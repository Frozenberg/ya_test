import unittest
from mymodule import func

class Test(unittest.TestCase):


    def test_1(self):
        str_in = ["", "abc", "123", "", "x", "", "", "y", "", ""]
        str_out = ["abc", "123", "", "x", "", "y"]

        result = list(func(str_in))
        self.assertEqual(result, str_out)

    def test_2(self):
        str_in = ["", "", "", "abc", "123", "", "x", "", "", "y", "", ""]
        str_out = ["abc", "123", "", "x", "", "y"]

        result = list(func(str_in))
        self.assertEqual(result, str_out)

    def test_3(self):
        str_in = ["abc", "123", "", "x", "", "", "", "", "y", "", ""]
        str_out = ["abc", "123", "", "x", "", "y"]

        result = list(func(str_in))
        self.assertEqual(result, str_out)

    def test_4(self):
        str_in = ["", "abc", "123", "", "x", "", "", "y", "", "", "", ""]
        str_out = ["abc", "123", "", "x", "", "y"]

        result = list(func(str_in))
        self.assertEqual(result, str_out)