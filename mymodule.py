def func(strings):
    first = True
    wait = False
    for i in strings:
        if first and not i: # пропускаем все пустые значения в начале списка
            continue
        else:
            first = False


        if i:
            if wait: # если мы ждали отправляем пустой элемент
                yield ''
            wait = False # ожидание завершилось нашли непустой элемент
            yield i # отправляем все непустые элементы
        elif not wait: # если элемент пустой ожидаем непустой
            wait = True

